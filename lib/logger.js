"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
const winston_1 = __importDefault(require("winston"));
const { format, transports, createLogger } = winston_1.default;
const levels = {
    error: 0,
    warn: 1,
    info: 2,
    http: 3,
    debug: 4,
};
const env = process.env["NODE_ENV"] || "development";
const level = () => {
    const logLevel = process.env["LOG_LEVEL"];
    if (logLevel) {
        return logLevel;
    }
    const isDevelopment = env === "development";
    return isDevelopment ? "debug" : "info";
};
exports.logger = createLogger({
    levels,
    level: level(),
    format: format.combine(format.timestamp(), format.simple(), env !== "production"
        ? format.colorize({
            all: true,
        })
        : format.uncolorize()),
    transports: [new transports.Console()],
    exitOnError: false,
});
