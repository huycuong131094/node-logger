import winston from "winston";

const { format, transports, createLogger } = winston;

const levels = {
  error: 0,
  warn: 1,
  info: 2,
  http: 3,
  debug: 4,
};

const env = process.env["NODE_ENV"] || "development";

const level = () => {
  const logLevel = process.env["LOG_LEVEL"];
  if (logLevel) {
    return logLevel;
  }
  const isDevelopment = env === "development";
  return isDevelopment ? "debug" : "info";
};

const logger = createLogger({
  levels,
  level: level(),
  format: format.combine(
    format.timestamp(),
    format.simple(),
    env !== "production"
      ? format.colorize({
          all: true,
        })
      : format.uncolorize()
  ),
  transports: [new transports.Console()],
  exitOnError: false,
});

export default logger;
