"use strict";
exports.__esModule = true;
var winston_1 = require("winston");
var format = winston_1["default"].format, transports = winston_1["default"].transports, createLogger = winston_1["default"].createLogger;
var levels = {
    error: 0,
    warn: 1,
    info: 2,
    http: 3,
    debug: 4
};
var env = process.env["NODE_ENV"] || "development";
var level = function () {
    var logLevel = process.env["LOG_LEVEL"];
    if (logLevel) {
        return logLevel;
    }
    var isDevelopment = env === "development";
    return isDevelopment ? "debug" : "info";
};
var logger = createLogger({
    levels: levels,
    level: level(),
    format: format.combine(format.timestamp(), format.simple(), env !== "production"
        ? format.colorize({
            all: true
        })
        : format.uncolorize()),
    transports: [new transports.Console()],
    exitOnError: false
});
exports["default"] = logger;
